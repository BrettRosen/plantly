//
//  GameScene.swift
//  Plantly
//
//  Created by Brett Rosen on 6/1/17.
//  Copyright © 2017 Brett Rosen. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    public var viewController: GameViewController!
    
    public let cameraNode = SKCameraNode()

    public var lsystem1: LSystem!
    public var lsystem2: LSystem!
    
    override func didMove(to view: SKView) {
        SceneCoordinator.shared.gameScene = self
        
        // Anchor at bottom left
        self.anchorPoint = CGPoint(x: 0, y: 0)
        
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.handlePinchFrom(_:)))
        self.view?.addGestureRecognizer(pinchGesture)

        lsystem1 = LSystem(axiom: "I", ruleSet: [
            Rule(a: "I", b: "IIR(RILILI)L(LIRIRI)", c: "IIR(RILILI)L(LILIRI)")
        ])
        
        lsystem2 = LSystem(axiom: "I", ruleSet: [
            Rule(a: "I", b: "IIL(LIRIRI)R(RILILI)", c: nil)
        ])
        
        
        cameraNode.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        addChild(cameraNode)
        camera = cameraNode
    }
    
    override func update(_ currentTime: TimeInterval) {

    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        lsystem2.generate()
    }
    
    func handlePinchFrom(_ sender: UIPinchGestureRecognizer) {
        let zoomInAction = SKAction.scale(by: sender.scale, duration: 0.0)
        cameraNode.run(zoomInAction)
        sender.scale = 1.0
        
    }
    
}
