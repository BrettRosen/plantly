//
//  LSystem.swift
//  Plantly
//
//  Created by Brett Rosen on 6/1/17.
//  Copyright © 2017 Brett Rosen. All rights reserved.
//

import Foundation
import SpriteKit

class LSystem {
    
    public var sentence: String
    public var ruleSet: [Rule]
    
    public var tree = [Branch]()
    public var startPoint = CGPoint(x: SceneCoordinator.shared.gameScene.frame.midX, y: 0)
    public var savePoint = CGPoint(x: SceneCoordinator.shared.gameScene.frame.midX, y: 0)
    public var generation = 0
    public var branchLength: CGFloat = 200.0
    public var angle = 0.0
    
    private var maxGeneration = 0
    
    public var stack = Stack()
    
    init(axiom: String, ruleSet: [Rule]) {
        sentence = axiom
        self.ruleSet = ruleSet
        
        calculateMaxGenerations(sentence: sentence, generation: generation)
        print(maxGeneration)
        turtle()
    }
    
    public func generate() {
        if generation < maxGeneration {
            branchLength *= 0.5
            angle = 0
            stack.stackArray = []
            startPoint = CGPoint(x: SceneCoordinator.shared.gameScene.frame.midX, y: 0)
            cleanTree()
            
            var nextSentence: String = ""
            
            for i in sentence.characters.indices {
                let current = sentence[i]
                var replace = "" + String(current)
                var found = false
                
                for j in 0..<ruleSet.count {
                    if ruleSet[j].a == current {
                        found = true
                        
                        if ruleSet[j].c != nil {
                            if RandomInt(1, max: 2) == 1 {
                                replace = ruleSet[j].c!
                            } else {
                                replace = ruleSet[j].b
                            }
                        } else {
                            replace = ruleSet[j].b
                        }
                        
                        break;
                    }
                }
                
                if !found {
                    nextSentence.append(current)
                } else {
                    nextSentence.append(replace)
                }
                
            }
            
            sentence = nextSentence
            generation += 1
            
            turtle()
            
        } else {
            print("Maximum generation reached.")
        }
        
    }
    
    private func turtle() {
        for (index, _) in sentence.characters.enumerated() {
            
            switch(sentence[index] as Character!) {
            case "I": // Straight line
                let branch = Branch(begin: startPoint, end: CGPoint(x: startPoint.x, y: startPoint.y + branchLength))
                branch.render()
                tree.append(branch)
                
                if index < sentence.characters.count - 1 {
                    if sentence[index+1] == ")" {
                        branch.line.lineWidth = 2
                        branch.line.strokeColor = UIColor(red: 38.0/255, green: 65.0/255, blue: 19.0/255, alpha: 1.0)
                    }
                }
               
                if angle != 0.0 {
                    branch.rotate(angle: angle)
                }
                
                startPoint = branch.getEndPoint()
            case "L": // Rotate left
                angle += Double(22.5).degreesToRadians
            case "R": // Rotate right
                angle -= Double(22.5).degreesToRadians
            case "(": // Save position
                stack.push(tuple: (startPoint, angle))
            case ")": // Restore position
                if let tuple = stack.pop() {
                    startPoint = tuple.0
                    angle = tuple.1
                }
            case "F":
                let fruit = Fruit(position: startPoint)
            default:
                print("Shouldn't reach this")
            }
        }
    }
    
    public func cleanTree() {
        for branch in tree {
            branch.line.removeFromParent()
        }
        tree = []
    }

        
    private func recolor() {
        for branch in tree {
            branch.line.strokeColor = UIColor.brown
        }
    }
    
    private func calculateMaxGenerations(sentence: String, generation: Int) {
        var sent = sentence
        var gen  = generation
        
        // Base case
        if sentence.characters.count >= 10000 {
            maxGeneration = gen
            return
        }
        
        var nextSentence: String = ""
        
        for i in sentence.characters.indices {
            let current = sentence[i]
            var replace = "" + String(current)
            var found = false
            
            for j in 0..<ruleSet.count {
                if ruleSet[j].a == current {
                    found = true
                    
                    if ruleSet[j].c != nil {
                        if RandomInt(1, max: 2) == 1 {
                            replace = ruleSet[j].c!
                        } else {
                            replace = ruleSet[j].b
                        }
                    } else {
                        replace = ruleSet[j].b
                    }
                    
                    break;
                }
            }
            
            if !found {
                nextSentence.append(current)
            } else {
                nextSentence.append(replace)
            }
        }
        
        sent = nextSentence
        gen += 1
        
        calculateMaxGenerations(sentence: sent, generation: gen)
    }
    
    
}

extension String {
    subscript (i: Int) -> Character {
        return self[self.characters.index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return self[start..<end]
    }
    
    subscript (r: ClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return self[start...end]
    }
}

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

func RandomInt(_ min: Int, max: Int) -> Int {
    if max < min { return min }
    return Int(arc4random_uniform(UInt32((max - min) + 1))) + min
}
