//
//  Branch.swift
//  Plantly
//
//  Created by Brett Rosen on 6/1/17.
//  Copyright © 2017 Brett Rosen. All rights reserved.
//

import Foundation
import SpriteKit

class Branch {
    
    public var begin: CGPoint
    public var end:   CGPoint
    
    public let line = SKShapeNode()
    private let linePath = CGMutablePath()

    init(begin: CGPoint, end: CGPoint) {
        self.begin = begin
        self.end = end
        
        linePath.move(to: CGPoint.zero)
        linePath.addLine(to: CGPoint(x: end.x - begin.x, y: end.y - begin.y))
        
        line.path = linePath
        line.position = begin
        line.strokeColor = UIColor.brown
        line.lineWidth = 2
        line.name = "branch"
        
        line.physicsBody = SKPhysicsBody(edgeChainFrom: linePath)
        line.physicsBody!.affectedByGravity = false
        
    }
    
    public func render() {
        SceneCoordinator.shared.gameScene.addChild(line)
    }
    
    public func rotate(angle: Double) {
        line.zRotation = CGFloat(angle)
    }
    
    public func getEndPoint() -> CGPoint {
        return line.convert(CGPoint(x: end.x - begin.x, y: end.y - begin.y), to: line.scene!)
    }

}
