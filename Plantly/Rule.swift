//
//  Rule.swift
//  Plantly
//
//  Created by Brett Rosen on 6/1/17.
//  Copyright © 2017 Brett Rosen. All rights reserved.
//

import Foundation

class Rule {
    
    public var a: Character
    public var b: String
    public var c: String?
    
    init(a: Character, b: String, c: String?) {
        self.a = a
        self.b = b
        self.c = c
    }
    
}
