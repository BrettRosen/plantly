//
//  SceneCoordinator.swift
//  Plantly
//
//  Created by Brett Rosen on 6/1/17.
//  Copyright © 2017 Brett Rosen. All rights reserved.
//

import Foundation
import SpriteKit

class SceneCoordinator {
    
    static var shared = SceneCoordinator()
    
    var gameScene: GameScene!
    
}
