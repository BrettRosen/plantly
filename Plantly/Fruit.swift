//
//  Fruit.swift
//  Plantly
//
//  Created by Brett Rosen on 6/2/17.
//  Copyright © 2017 Brett Rosen. All rights reserved.
//

import Foundation
import SpriteKit

class Fruit {
    
    public var position: CGPoint!
    
    public let circle = SKShapeNode(circleOfRadius: 5.0)
    
    init(position: CGPoint) {
        self.position = position
        
        if RandomInt(1, max: 2) == 1 {
            circle.fillColor = UIColor.red
        } else {
            circle.fillColor = UIColor.blue
        }
        
        circle.strokeColor = UIColor.black
        circle.lineWidth = 2
        circle.position = position
        
        SceneCoordinator.shared.gameScene.addChild(circle)
        
    }
    
}
