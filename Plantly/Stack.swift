//
//  Stack.swift
//  Plantly
//
//  Created by Brett Rosen on 6/2/17.
//  Copyright © 2017 Brett Rosen. All rights reserved.
//

import Foundation
import SpriteKit

class Stack {
    
    public var stackArray = [(CGPoint, Double)]()
    
    func push(tuple: (CGPoint, Double)){
        stackArray.append(tuple)
    }
    
    func pop() -> (CGPoint, Double)? {
        if stackArray.last != nil {
            let tuple = stackArray.last
            stackArray.removeLast()
            return tuple!
        } else {
            return nil
        }
    }
    
}
